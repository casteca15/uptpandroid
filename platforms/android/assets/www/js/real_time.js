$(document).on('ready', function(){
    var url = "http://uptpandroid-uptpapi.rhcloud.com/";
    //var url = "http://127.0.0.1:8080/";

    if(localStorage.user){
        iniciarSesion(localStorage.user, localStorage.pas);
    }

    $( ".photopopup" ).on({
        popupbeforeposition: function() {
            var maxHeight = $( window ).height() - 60 + "px";
            $( ".photopopup img" ).css( "max-height", maxHeight );
        }
    });

    $("#bt_buscar_cedula").on('click', function(){
        var ced = $("#ced").val().trim();
        if(ced.length > 5){
            $.mobile.loading( "show", {
                text: "Cargando . . .",
                textVisible: true,
                theme: "b",
                html: ""
            });
            $.ajax({
                type: "GET",
                url: url+"alumno/"+ced,
                dataType: "json",
                timeout: 15000,
                error: function(e){
                }, 
                success: function(data){
                    $.mobile.loading("hide");
                    if(data != null){
                        $("input[name='nom']").val(data.nombres_a);
                        $("input[name='ape']").val(data.apellidos_a);
                        if(data.estado_a == 'activo')
                            alert("La cedula facilitada ya tiene cuenta en la App");
                        else
                            $("#btg").button('enable');
                    }else{
                        alert("No hay usuario, con la cedula que nos facilito")
                        $("input[name='nom']").val("");
                        $("input[name='ape']").val("");
                        $("#btg").button('disable');
                    }
                }
            });
        }else{
            alert("cedula muy corta")
        }
    });

    $("#form_registro").on('submit', function(e){
        var pa = $("input[name='pass']").val().trim();
        var cp = $("input[name='cpas']").val().trim();

        if(pa.length < 5 || cp.length < 5){
            alert("La contraseña debe tener almenos 5 caracteres");
            return false;
        }
        
        if(pa != cp){
            alert("Las contraseñas no son iguales");
            return false;
        }

        $.ajax({
            type: "POST",
            url: url+"nuevo/alumno",
            data: $("#form_registro").serialize(),
            async: true,
            dataType: "json",
            timeout: 15000,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
            },
            success: function(data){
                if(data.r == 'encontrado'){
                    alert("Ya esta cedula, tiene su cuenta creada");
                }else if(data.r == 'ok'){
                    $("#form_registro")[0].reset();
                    alert("Se ha creado su cuenta correctamente.");
                    $.mobile.changePage("#vt_sesion", { transition: "slideup", changeHash: true });
                }else if(data.r == 'no'){
                    alert("No se pudo crear la cuenta, contacte al administrador");
                }else if(data.r == 'noencontrado'){
                    alert("No existe esta cedula en la data de la uptp");
                }
            }
        });
        return false;
    });

    $("#form_sesion").on('submit', function(e){
        var u = $("input[name='usuario']").val().trim();
        var p = $("input[name='clave']").val().trim();
        iniciarSesion(u, p);
        return false;
    });

    $(".btsalir").on('click', function(){
        if(confirm("Esta seguro?")){
            localStorage.clear();
            // Cerrar sesion en la api
            $.mobile.changePage("#vt_sesion");
            navigator.app.exitApp();
        }
        return false;
    });

    $(".btacerca").on('click', function(){
        $.mobile.changePage("#vt_acerca", { transition: "slideup", changeHash: true });
        return false;
    });

    $(".btperfil").on('click', function(){
        $.mobile.changePage("#vt_perfil", { transition: "slidedown", changeHash: true });
        return false;
    });

    $(".btchat").on('click', function(){
        $(".btchat").css('background-color', '#333');
        $.mobile.changePage("#vt_chat", { transition: "slide", changeHash: true });
        return false;
    });

    $(document).on('pageshow', '#vt_perfil', function(){
        actualizarMiPerfil();
    });

    $(document).on('pageshow', '#vt_recientes', function(){
        cargarDatosUsuario();
        actualizarMiPerfil();
    });

    $(document).on('pageshow', '#vt_noticias', function(){
        obtenerNoticias();
    });

    function actualizarMiPerfil(){
        if($("#img_perfil").attr('src') != url+localStorage.img){
            $("#img_perfil").attr('src', url+localStorage.img);
            $("#ver_foto_perfil").attr('src', url+localStorage.img);
        }
        $("#nombre_perfil").text(localStorage.nombre);
        $("#turno_perfil").text(localStorage.turno);
        $("#sexo_perfil").text(localStorage.sexo);
        $("#tipo_perfil").text(localStorage.tipo);
        $("#sede_perfil").text(localStorage.sede);
        $("#carrera_perfil").text(localStorage.carrera);
    }

    function iniciarSesion(u, p){
        $.ajax({
            type: "POST",
            url: url+"sesion",
            data: {
                usuario: u,
                pas: p
            },
            async: true,
            dataType: "json",
            timeout: 15000,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
            },
            success: function(data){
                if(data == null){
                    alert("Usuario o contraseña invalido");
                }else{
                    $("#caja_user").val('');
                    $("#caja_pas").val('');
                    localStorage.user = u;
                    localStorage.pas = p;
                    localStorage.cedula_a = data.cedula_a;
                    localStorage.carrera = data.nombre_ca;
                    localStorage.nombre = data.nombres_a+" "+data.apellidos_a;
                    localStorage.soloNombre = data.nombres_a;
                    localStorage.img = data.img_a;
                    localStorage.turno = data.turno_a;
                    localStorage.sexo = data.sexo_a;
                    localStorage.sede = data.sede_a;
                    localStorage.tipo = data.tipoa_a;
                    $.mobile.changePage("#vt_recientes", { transition: "slide", changeHash: true});
                }
            }
        });
    }

    function cargarDatosUsuario(){
        $.ajax({
            type: "POST",
            url: url+"sesion",
            data: {
                usuario: localStorage.user,
                pas: localStorage.pas
            },
            async: true,
            dataType: "json",
            timeout: 15000,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
            },
            success: function(data){
                if(data == null){
                    localStorage.clear();
                    $.mobile.changePage("#vt_sesion", { transition: "slide", changeHash: true});
                    alert("Usuario o contraseña invalido");
                }else{
                    localStorage.cedula_a = data.cedula_a;
                    localStorage.id_carrera = data.id_carrera;
                    localStorage.nombre = data.nombres_a+" "+data.apellidos_a;
                    localStorage.img = data.img_a;
                    localStorage.soloNombre = data.nombres_a;
                    localStorage.turno = data.turno_a;
                    localStorage.sexo = data.sexo_a;
                    localStorage.sede = data.sede_a;
                    localStorage.tipo = data.tipoa_a;
                }
            }
        });
    }

    function obtenerNoticias(){
        $.mobile.loading( "show", {
            text: "Cargando . . .",
            textVisible: true,
            theme: "b",
            html: ""
        });
        $("#contenedor_noticias").html('');
        $.ajax({
            type: "GET",
            url: url+"obtener/noticias",
            dataType: "json",
            timeout: 15000,
            error: function(){
            }, 
            success: function(data){
                $.mobile.loading("hide");
                var html = "";
                for(i in data){
                    var o = data[i];
                    html+= "<div data-role='popup' id='foto_de_noticia_"+o.id_noticia+"' class='photopopup' data-overlay-theme='a' data-corners='false' data-tolerance='30,15'>";
                    html+= "<img id='ver_foto_noticia' class='fotico_noticia' src='"+url+o.img_no+"'></div>";
                    html+= "<div data-role='collapsible' data-content-theme='b'>";

                    html+= "<h3>"+o.titulo_no;
                    html+= "<br><span class='texto_minuscula fecha_mensaje timeago' title='"+o.fecha_no+"'></span></h3>";
                    html+= "<div style='text-align:center'><h3>"+o.titulo_no+"</h3>";
                    html+= "<a href='#foto_de_noticia_"+o.id_noticia+"' data-rel='popup' data-position-to='window' class='' data-transition='fade' data-theme='b'>";
                    html+= "<img class='ver_foto_noticia' src='"+url+o.img_no+"'></a></div>";
                    html+= "<p style='text-align: justify;text-transform: uppercase;'>"+o.contenido_no+"</p>";
                    if(o.video_no != ""){
                        html+= "<iframe width='100%' height='215' src='https://www.youtube.com/embed/"+o.video_no+"?rel=0&amp;controls=1&amp;showinfo=0&amp;modestbranding=1' frameborder='0' allowfullscreen></iframe><br><br>";
                    }
                    html+= "<span>NOTICIA PUBLICADA POR: "+o.nombre_co+"</span><br>";
                    html+= "<b><span class='texto_minuscula fecha_mensaje timeago' title='"+o.fecha_no+"'></span></b>";
                    html+= "</div>";
                }
                $("#contenedor_noticias").append($(html)).trigger('create');
            }
        });
    }

    $(document).on('tap', ".ver_foto_noticia", function(){
        $(".fotico_noticia").trigger('create');
    });
});
