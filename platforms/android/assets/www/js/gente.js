$(document).on('ready', function(){

    var url = "http://uptpandroid-uptpapi.rhcloud.com/";
    //var url = "http://127.0.0.1:8080/";

    $(document).on('pageshow', '#vt_gente', function(){
        obtenerGente();
    });

    function obtenerGente(){
        $("#listarGente").html('');
        $.ajax({
            type: "GET",
            url: url+"obtener/gente",
            dataType: "json",
            timeout: 15000,
            error: function(){
            }, 
            success: function(data){
                $("#total_estudiantes").text('USUARIOS EN LA APP (' + data.length+")");
                for(i in data){
                    ponerGente(data[i]);
                }
                $('#listarGente').listview('refresh');
            }
        });
    }

    function ponerGente(data){
        var html = "";
        html+= "<li class='ui-li-has-thumb'>";
        html+= "    <a href='' class='ui-btn ver_perfil_de_otro' id='"+data.cedula_a+"'>";
        html+= "        <img src='"+(url+data.img_a)+"' class='ui-li-thumb'>";
        html+= "        <h2>"+data.nombres_a+" "+data.apellidos_a+"</h2>";
        html+= "        <p> "+ data.tipoa_a +" - "+ data.sede_a+"</p>";
        html+= "    </a>";
        html+= "</li>";
        $("#listarGente").append(html);
    }
});
