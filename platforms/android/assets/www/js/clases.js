$(document).on('ready', function(){
    var url = "http://uptpandroid-uptpapi.rhcloud.com/";
    //var url = "http://127.0.0.1:8080/";

    $(document).on('click', ".btclases", function(){
        $.mobile.changePage("#vt_clases", { transition: "slide",  changeHash: true });

        $.mobile.loading( "show", {
            text: "Cargando . . .",
            textVisible: true,
            theme: "b",
            html: ""
        });

        var id = localStorage.getItem("id_carrera");
        $("#listaMaterias").html('');
        $.ajax({
            type: "GET",
            url: url+"materias/"+id,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            timeout: 15000,
            error: function(){
            },
            success: function(data){
                $.mobile.loading("hide");
                $("#listaMaterias").html('');
                for(i=0; i < data.length; i++){
                    var obj = data[i];
                    if(obj.filas == null)
                        obj.filas = 0;
                    var html = "<li class='btMateria' id='"+obj.id_materia+"'><a>"+obj.nombre_ma+"<span class='ui-li-count ui-btn-up-c ui-btn-corner-all'>"+obj.filas+"</span></a></li>";
                    $("#listaMaterias").append(html);
                }
                $('#listaMaterias').listview('refresh');
            }
        });
        return false;
    });

    $(document).on('click', '.btMateria', function(){
        $.mobile.changePage("#vt_clases_materia", { transition: "slide",  changeHash: true });
        $.mobile.loading( "show", {
            text: "Cargando . . .",
            textVisible: true,
            theme: "b",
            html: ""
        });

        var id = this.id;
        $("#listaClases").html('');
        $.ajax({
            type: "GET",
            url: url+"clases/"+id,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            timeout: 15000,
            error: function(){
            },
            success: function(data){
                $.mobile.loading("hide");
                $("#listaClases").html('');
                for(i=0; i < data.length; i++){
                    var obj = data[i];
                    var html = "<li class='btClase' id='"+obj.id_clase+"'><a>"+obj.nombre_cla+"</a></li>";
                    $("#listaClases").append(html);
                }
                $('#listaClases').listview('refresh');
            }
        });
        return false;
    });

    $(document).on('click', '.btClase', function(){
        $.mobile.changePage("#vt_ver_clase", { transition: "slide",  changeHash: true });
        var id = this.id;

        $.mobile.loading( "show", {
            text: "Cargando . . .",
            textVisible: true,
            theme: "b",
            html: ""
        });

        $.ajax({
            type: "GET",
            url: url+"clase/"+id,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            timeout: 15000,
            error: function(){
            },
            success: function(data){
                $("#titulo_clase").text(data.nombre_cla);
                var url = "http://www.youtube.com/embed/"+data.ruta+"?rel=0&amp;controls=1&amp;showinfo=0&amp;modestbranding=1";
                if($("#id_video_clase").attr('src') != url){
                    $("#id_video_clase").attr('src', url);
                }
                $.mobile.loading("hide");
            }
        });
        return false;
    });

    $(".bt_ver_video").on('click', function(){
        var id = this.id;
        var ref = window.open('https://www.youtube.com/embed/'+id+'?html5=1', '_blank', 'location=no, menubar=no, toolbar=no, scrollbars=no, status=no, resizable=yes,');
    });
});
