$(document).on('ready', function(){
    var url = "http://uptpandroid-uptpapi.rhcloud.com/";
    //var url = "http://127.0.0.1:8080/";
    var recargar = 0;

    var web = io.connect(url);

    web.on('recibir_mensaje', recibirMensaje);
    web.on('recibir_reciente', function(data){
        if(data.ced != localStorage.user)
            $(".btreciente").css('color', 'red');
        recibirReciente(data, 1);
    });

    $(document).on('pageshow', '#vt_recientes', function(){
        $(".btreciente").css('color', '#fff');
        recargar++;
        if(recargar == 15){
            cargarRecientes();
            recargar = 0;
        }
    });

    $(document).on('tap', '.ver_perfil_de_otro', function(){
        var ced = $(this).attr('id');
        $(".btvoto_del").attr('id', ced);
        $(".btvoto_che").attr('id', ced);
        $(".btvoto_hea").attr('id', ced);

        $.mobile.changePage("#vt_perfil_otro", { transition: "slide", changeHash: true});
        verPerfilOtro(ced);
    });

    $(document).on('pageshow', '#vt_perfil', function(){
        $(".btvoto_del").attr('id', '');
        $(".btvoto_che").attr('id', '');
        $(".btvoto_hea").attr('id', '');
        buscarVotos(localStorage.user);
    });

    $("#txt_chat").on('focus', function(){
        $.mobile.silentScroll($("#mover_scroll_aqui").offset().top);
    });

    $("#enviar_mensaje").on('click', function(){
        var t = $("#txt_chat").val().trim();
        $("#txt_chat").val('');

        if(t.length < 3){
            alert("Mensaje muy corto, minimo 3 letras");
            return;
        }
        $("#txt_chat").attr('disabled', true);
        setTimeout(function(){
            $("#txt_chat").attr('disabled', false);
        }, 5000);
        enviarMensaje(t);
    });

    $(document).on('pagebeforecreate', '#vt_recientes', function(){
        obtenerMensajes();
        cargarRecientes();
    });

    $(document).on('pageshow', '#vt_chat', function(){
        if($("#img_perfil").attr('src') != url+localStorage.img){
            obtenerMensajes();
        }
        if($("#cont_chat").html().trim().length == 0){
        }
        $.mobile.silentScroll($("#mover_scroll_aqui").offset().top);
    });

    function enviarMensaje(t){
        $.ajax({
            type: "POST",
            url: url+"nuevo/mensaje",
            data: {
                msj: t,
                id: localStorage.cedula_a,
                img: localStorage.img,
                nom: localStorage.nombre
            },
            async: true,
            dataType: "json",
            timeout: 15000,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
            },
            success: function(data){
                if(data.r == 'ok'){
                    
                }else if(data.r == 'no'){
                    alert("Su mensaje no se pudo enviar");
                }
            }
        });
    }


    function recibirMensaje(data){
        if(data.ced != localStorage.user){
            $(".btchat").css('background-color', 'red');
        }

        var html = "";
        html+= "<div class='contenedor_mensaje_completo ver_perfil_de_otro' id='"+data.ced+"'>";
        html+= "    <span class='txt_en_linea'>"+data.nom+"</span>";
        html+= "    <br>";
        html+= "    <div class='ui-block-a izq_foto'  style='padding-right:0.5em;'>";
        html+= "        <img src='"+url+data.img+"' class='img_thumb'> ";
        html+= "    </div>";
        html+= "    <div class='ui-block-b der_foto'>";
        html+= "    </div>";
        html+= "    <p class='cont_texto_msj texto_minuscula'>";
        html+= data.msj;
        html+= "    </p>";
        html+= "    <span class='texto_minuscula fecha_mensaje timeago' title='"+data.fecm+"'></span>";
        html+= "</div><br><hr>";
        $("#cont_chat").append(html);
        $.mobile.silentScroll($("#mover_scroll_aqui").offset().top);
    }

    function obtenerMensajes(){
        $("#cont_chat").html('');
        $.ajax({
            type: "GET",
            url: url+"obtener/mensajes",
            dataType: "json",
            timeout: 15000,
            error: function(){
            }, 
            success: function(data){
                for(i in data){
                    var o = data[i];
                    var html = "";
                    html+= "<div class='contenedor_mensaje_completo ver_perfil_de_otro' id='"+o.cedula_a+"'>";
                    html+= "    <span class='txt_en_linea'>"+o.nombres_a+" "+o.apellidos_a+"</span>";
                    html+= "    <br>";
                    html+= "    <div class='ui-block-a izq_foto' style='padding-right:0.5em;'>";
                    html+= "        <img src='"+url+o.img_a+"' class='img_thumb'> ";
                    html+= "    </div>";
                    html+= "    <div class='ui-block-b der_foto'>";
                    html+= "    </div>";
                    html+= "    <p class='cont_texto_msj texto_minuscula'>"+o.msj+"</p>";
                    html+= "    <span class='texto_minuscula fecha_mensaje timeago' title='"+o.fec_msjs+"'></span>";
                    html+= "</div><br><hr>";
                    $("#cont_chat").prepend(html);
                }
                $.mobile.silentScroll($("#mover_scroll_aqui").offset().top);
            }
        });
    }

    function verPerfilOtro(id){

            $.mobile.loading( "show", {
                text: "Cargando . . .",
                textVisible: true,
                theme: "b",
                html: ""
            });
            $.ajax({
                type: "GET",
                url: url+"alumno/"+id,
                dataType: "json",
                timeout: 15000,
                error: function(e){
                }, 
                success: function(data){
                    $.mobile.loading("hide");
                    if(data != null){
                        if($("#img_perfil_otro").attr('src') != url+data.img_a){
                            $("#img_perfil_otro").attr('src', url+data.img_a);
                            $("#ver_foto_perfil_otro").attr('src', url+data.img_a);
                        }
                        $("#nombre_perfil_otro").text(data.nombres_a+" "+data.apellidos_a);
                        $("#name_oculto_perfil").val(data.nombres_a);
                        $("#img_oculto_perfil").val(data.img_a);
                        $("#turno_perfil_otro").text(data.turno_a);
                        $("#sexo_perfil_otro").text(data.sexo_a);
                        $("#sede_perfil_otro").text(data.sede_a);
                        $("#tipo_perfil_otro").text(data.tipoa_a);
                        $("#carrera_perfil_otro").text(data.nombre_ca);
                    }else{
                        alert("Perfil no disponible");
                        $.mobile.back();
                    }
                }
            });

        buscarVotos(id);
    }

    function buscarVotos(id){
        $(".btvoto_hea").text(0);
        $(".btvoto_che").text(0);
        $(".btvoto_del").text(0);

        $.ajax({
            type: "GET",
            url: url+"votos/"+id+"/"+localStorage.user,
            dataType: "json",
            timeout: 15000,
            error: function(e){
            }, 
            success: function(data){
                console.log(data);
                cargarVotos(data);
            }
        });
    }

    function cargarVotos(data){
        $(".btvoto_hea").css('background-color', '#f6f6f6');
        $(".btvoto_che").css('background-color', '#f6f6f6');
        $(".btvoto_del").css('background-color', '#f6f6f6');
        for(i in data.votos){
            var o = data.votos[i];

            if(o.voto == 2)
                $(".btvoto_hea").text(o.count);
            else if(o.voto == 1)
                $(".btvoto_che").text(o.count);
            else if(o.voto == 0)
                $(".btvoto_del").text(o.count);
        }

        if(data.voto && data.voto.length == 1){
            var v = data.voto[0].voto;
            if(v == 2)
                $(".btvoto_hea").css('background-color', '#FFC5C3');
            else if(v == 1)
                $(".btvoto_che").css('background-color', '#87D18B');
            else if(v == 0)
                $(".btvoto_del").css('background-color', '#E5E0E0');
        }
    }

    function cargarRecientes(){
        $("#contenedor_recientes").html('');
        $.ajax({
            type: "GET",
            url: url+"obtener/recientes",
            dataType: "json",
            timeout: 15000,
            error: function(){
            }, 
            success: function(data){
                var html = "";
                for(i in data){
                    recibirReciente(data[i], 0);
                }
            }
        });
    }

    function recibirReciente(data, v){
        var html = "";
        html+= "<div class='ver_perfil_de_otro' style='display:none;' id='"+data.ced+"'>";
        html+= "    <div class='ui-block-a izq_foto' style='padding-right: 0.5em;'>";
        html+= "        <img src='"+url+data.img+"' class='img_thumb'> ";
        html+= "    </div>";
        html+= "    <div class='ui-block-b der_foto'>";
        html+= "    </div>";
        html+= "    <p class='cont_texto_msj texto_minuscula'>";
        html+= "     <b>"+data.nom+": </b>"+ data.msj;
        html+= "    </p>";
        html+= "    <span class='texto_minuscula fecha_mensaje timeago' title='"+data.fecm+"'></span>";
        html+= "</div><br><hr style='background-color: #ED343B;height: 1px;border:none;'>";
        if(v == 0){
            $("#contenedor_recientes").append(html);
            $("div.ver_perfil_de_otro").show();
        }else{
            $("#contenedor_recientes").prepend(html);
            $("div.ver_perfil_de_otro").slideDown('slow');
        }
    }
});




