$(document).on('ready', function(){

    var url = "http://uptpandroid-uptpapi.rhcloud.com/";
    //var url = "http://127.0.0.1:8080/";
    
    $(document).on('tap', '.btvoto_del', function(){
        var ced_i = localStorage.user;
        var ced_f = this.id;
        if(ced_f == "" || ced_f == localStorage.user)
            return
        votar(ced_i, ced_f, 0);
    });
    $(document).on('tap', '.btvoto_che', function(){
        var ced_i = localStorage.user;
        var ced_f = this.id;
        if(ced_f == "" || ced_f == localStorage.user)
            return
        votar(ced_i, ced_f, 1);
    });
    $(document).on('tap', '.btvoto_hea', function(){
        var ced_i = localStorage.user;
        var ced_f = this.id;
        if(ced_f == "" || ced_f == localStorage.user)
            return
        votar(ced_i, ced_f, 2);
    });

    $(document).on('tap', '.ver_voto_del', function(data){
        obtenerVotos(localStorage.user, 0);
    });
    $(document).on('tap', '.ver_voto_che', function(data){
        obtenerVotos(localStorage.user, 1);
    });
    $(document).on('tap', '.ver_voto_hea', function(data){
        obtenerVotos(localStorage.user, 2);
    });

    function obtenerVotos(ced, voto){
        $.mobile.changePage("#vt_votos", { transition: "slide", changeHash: true});
        $("#listarGenteVoto").html('');
        $.ajax({
            type: "GET",
            url: url+"obtener/votos/"+ced+"/"+voto,
            dataType: "json",
            timeout: 15000,
            error: function(){
            }, 
            success: function(data){
                if(voto == 0)
                    txt = "<input type='button' data-icon='delete' id='btfoto' style='background-color: #e5e0e0;' data-role='button' value='"+data.gente.length+"'>";
                if(voto == 1)
                    txt = "<input type='button' data-icon='check' id='btfoto' style='background-color: #87d18b;' data-role='button' value='"+data.gente.length+"'>";
                if(voto == 2)
                    txt = "<input type='button' data-icon='heart' id='btfoto' style='background-color: #FFC5C3;' data-role='button' value='"+data.gente.length+"'>";
                $("#titulo_votos").html(txt).trigger('create');

                for(i in data.gente){
                    ponerVoto(data.gente[i]);
                    console.log(data.gente[i]);
                }
                $('#listarGenteVoto').listview('refresh');
            }
        });
    }

    function ponerVoto(data){
        var html = "";
        html+= "<li class='ui-li-has-thumb'>";
        html+= "    <a href='' class='ui-btn ver_perfil_de_otro' id='"+data.cedula_a+"'>";
        html+= "        <img src='"+(url+data.img_a)+"' class='ui-li-thumb'>";
        html+= "        <h2>"+data.nombres_a+" "+data.apellidos_a+"</h2>";
        html+= "        <p> "+ data.tipoa_a +" - "+ data.sede_a+"</p>";
        html+= "    </a>";
        html+= "</li>";
        $("#listarGenteVoto").append(html);
    }

    function votar(ced_i, ced_f, voto){
        $.ajax({
            type: "POST",
            url: url+"nuevo/voto",
            data: {
                ced_i: ced_i,
                ced_f: ced_f,
                nom: $("#name_oculto_perfil").val(),
                img: $("#img_oculto_perfil").val(),
                voto: voto
            },
            async: true,
            dataType: "json",
            timeout: 15000,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
            },
            success: function(data){
                if(data.voto == 'ok'){
                    alert("Su opinion sobre este usuario, fue almacenada");
                    $.mobile.back();
                }
            }
        });
    }

});
